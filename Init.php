<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebCookies;

use FacturaScripts\Core\Base\InitClass;
use FacturaScripts\Plugins\WebCookies\Model\CookieCategory;
use FacturaScripts\Plugins\WebCookies\Model\Cookie;
use FacturaScripts\Dinamic\Model\Settings;

/**
 * Description of Init
 *
 * @author Athos Online <info@athosonline.com>
 */
class Init extends InitClass
{

    public function init()
    {
        if (WEBMULTILANGUAGE) {
            $this->addSettings(['cookieinfo']);
        }

        $this->loadExtension(new Extension\Controller\WebCreator());
    }

    public function update()
    {
        $this->configure();
        new CookieCategory();
        new Cookie();

        if (WEBMULTILANGUAGE) {
            $this->addSettings(['cookieinfo']);
            $modelTranslate = '\\FacturaScripts\\Dinamic\\Lib\\Portal\\TranslatePlugins';
            $modelTranslate::checkPlugin('WebCookies');
            $modelTranslate::translateSettings();
        }
    }

    public function addSettings($values)
    {
        foreach ($values as $value) {
            if (!in_array($value, Settings::$fieldsTranslate)) {
                array_push(Settings::$fieldsTranslate, $value);
            }
        }
    }

    public function configure()
    {
        $appSettings = $this->toolBox()->appSettings();
        $appSettings->set('webcreator', 'cookiestyle', 1);
        $appSettings->set('webcreator', 'cookiebackground', '#ffffff');
        $appSettings->set('webcreator', 'cookiebackgroundopacity', 1);
        $appSettings->set('webcreator', 'cookiecolortext', '#1e1e1e');
        $appSettings->set('webcreator', 'cookiebackgroundoverlay', '#000000');
        $appSettings->set('webcreator', 'cookieinfo', 'Usamos cookies en nuestro sitio web para brindarte la experiencia más relevante recordando tus preferencias y visitas repetidas. Al hacer clic en "Aceptar", aceptas el uso de TODAS las cookies necesarias.');
        $appSettings->save();
    }
}