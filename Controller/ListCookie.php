<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebCookies\Controller;

use FacturaScripts\Core\Lib\ExtendedController;

/**
 * Description of ListCookie
 *
 * @author Athos Online <info@athosonline.com>
 */
class ListCookie extends ExtendedController\ListController
{
    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $data = parent::getPageData();
        $data['menu'] = 'web';
        $data['title'] = 'cookies';
        $data['icon'] = 'fas fa-cookie';
        return $data;
    }
    
    protected function createViews() {
        $this->createViewsCookies();
        $this->createViewsCategories();
    }
    
    /**
     * 
     * @param string $viewName
     */
    protected function createViewsCookies(string $viewName = 'ListCookie')
    {
        $this->addView($viewName, 'Cookie', 'cookie', 'fas fa-cookie');
    }
    
    /**
     * 
     * @param string $viewName
     */
    protected function createViewsCategories(string $viewName = 'ListCookieCategory')
    {
        $this->addView($viewName, 'CookieCategory', 'categories', 'fas fa-list-alt');
    }
}