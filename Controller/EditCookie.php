<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebCookies\Controller;

use FacturaScripts\Dinamic\Lib\ExtendedController\PanelController;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Model\Cookie;

/**
 * Description of EditCookie
 *
 * @author Athos Online <info@athosonline.com>
 */
class EditCookie extends PanelController
{

    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['menu'] = 'web';
        $pagedata['title'] = 'cookie';
        $pagedata['icon'] = 'fas fa-cookie';
        $pagedata['showonmenu'] = false;
        return $pagedata;
    }

    /**
     * Load views.
     */
    protected function createViews()
    {
        $this->addEditView('EditCookie', 'Cookie', 'cookie', 'fas fa-cookie');

        if (WEBMULTILANGUAGE) {
            $this->addEditListView('EditCookieTranslate', 'WebTranslate', 'translations', 'fa fa-language');
            $this->setTabsPosition('top');
        }
    }

    protected function loadData($viewName, $view) {
        switch ($viewName) {
            case 'EditCookie':
                $code = $this->request->get('code');
                $view->loadData($code);
                break;
                
            case 'EditCookieTranslate':
                $modelLanguage = '\\FacturaScripts\\Dinamic\\Model\\WebLanguage';
                // Languages
                $columnLanguages = $this->views['EditCookieTranslate']->columnForName('language');
                if ($columnLanguages && $columnLanguages->widget->getType() === 'select') {
                    $customValues = [];
                    foreach ($modelLanguage::getWebLanguages() as $lang) {
                        $customValues[] = [
                            'value' => $lang->codicu,
                            'title' => $lang->name
                        ];
                    }
                    $columnLanguages->widget->setValuesFromArray($customValues);
                }

                // Keys Fields translates
                $columnKeys = $this->views['EditCookieTranslate']->columnForName('key');
                if ($columnKeys && $columnKeys->widget->getType() === 'select') {
                    $customValues = [];
                    foreach (Cookie::$fieldsTranslate as $field) {
                        $customValues[] = [
                            'value' => $field,
                            'title' => $field
                        ];
                    }
                    $columnKeys->widget->setValuesFromArray($customValues);
                }

                $where = [
                    new DataBaseWhere('modelid', $this->request->get('code')),
                    new DataBaseWhere('modelname', 'Cookie')
                ];
                $view->loadData('', $where);
                break;
        }
    }
}