<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebCookies\Controller;

use FacturaScripts\Core\Base\Controller;
use FacturaScripts\Dinamic\Model\CookieCategory;
use FacturaScripts\Dinamic\Model\Cookie as WebCookie;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Description of CookieController
 *
 * @author Athos Online <info@athosonline.com>
 */
class CookieController extends Controller
{
    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['menu'] = 'web';
        $pagedata['title'] = 'cookie';
        $pagedata['icon'] = 'fas fa-cookie';
        $pagedata['showonmenu'] = false;
        return $pagedata;
    }

    /**
     * 
     * @param type $response
     */
    public function publicCore(&$response)
    {
        parent::publicCore($response);
        $this->execPreviousAction($this->request->request->get('action'));
    }
    
    /**
     * 
     * @param type $response
     * @param type $user
     * @param type $permissions
     */
    public function privateCore(&$response, $user, $permissions)
    {
        parent::privateCore($response, $user, $permissions);
        $this->execPreviousAction($this->request->request->get('action'));
    }
    
    protected function execPreviousAction($action)
    {
        switch ($action) {
            case 'getCookiesCategories':
                $this->setTemplate(false);
                $this->response->setContent(json_encode($this->getCookiesCategories()));
                return false;
                
            case 'acceptCookie':
                $this->setTemplate(false);
                $this->acceptCookie();                
                $this->response->setContent(json_encode(['status' => true]));
                return false;
                
            case 'modalAcceptCookie':
                $this->setTemplate(false);
                $this->modalAcceptCookie();
                $this->response->setContent(json_encode(['status' => true]));
                return false;
        }
    }

    protected function acceptCookie()
    {
        $categoriesModel = new CookieCategory();
        $where = [new DataBaseWhere('necesary', 1)];
        
        $cookiesAccept = [];
        foreach ($categoriesModel->all($where, [], 0, 0) as $c) {
            array_push($cookiesAccept, $c->idcategory);
        }
        
        $this->saveCookie($cookiesAccept);
    }

    protected function modalAcceptCookie()
    {
        $categories = $this->request->request->get('categories');
        
        $cookiesAccept = [];
        foreach ($categories as $c) {
            if ($c['accept']) {
                array_push($cookiesAccept, $c['idcategory']);
            }
        }

        $this->saveCookie($cookiesAccept);
    }

    protected function getCookiesCategories()
    {
        if (WEBLANGUAGE) {
            return $this->getCookiesCategoriesTranslate();
        } else {
            $categoriesModel = new CookieCategory();
            $categories = $categoriesModel->all([], [], 0, 0);
            
            foreach ($categories as $c) {
                $cookiesModel = new WebCookie();
                $where = [new DataBaseWhere('idcategory', $c->idcategory)];
                $c->cookies = $cookiesModel->all($where, [], 0, 0);
            }
    
            return $categories;
        }
    }

    protected function getCookiesCategoriesTranslate()
    {
        $translateClass = '\\FacturaScripts\\Dinamic\\Model\\WebTranslate';
        $languageClass = '\\FacturaScripts\\Dinamic\\Model\\Weblanguage';
        
        $categoriesModel = new CookieCategory();
        $categories = $categoriesModel->all([], [], 0, 0);
        
        foreach ($categories as $c) {
            $categoryTranslate = new $translateClass();
            $whereCT = [
                new DataBaseWhere('modelid', $c->idcategory),
                new DataBaseWhere('modelname', 'CookieCategory'),
                new DataBaseWhere('codicu', $languageClass::getWebLangFile())
            ];
            
            foreach ($categoryTranslate->all($whereCT, [], 0, 0) as $ct) {
                if ($ct->keytrans == 'name') {
                    $c->name = $ct->valuetrans;
                }

                if ($ct->keytrans == 'description') {
                    $c->description = $ct->valuetrans;
                }
            }
            
            $cookiesModel = new WebCookie();
            $whereCO = [new DataBaseWhere('idcategory', $c->idcategory)];
            $cookies = $cookiesModel->all($whereCO, [], 0, 0);
            foreach ($cookies as $co) {
                $cookieTranslate = new $translateClass();
                $whereCOT = [
                    new DataBaseWhere('modelid', $co->idcookie),
                    new DataBaseWhere('modelname', 'Cookie'),
                    new DataBaseWhere('codicu', $languageClass::getWebLangFile())
                ];

                foreach ($cookieTranslate->all($whereCOT, [], 0, 0) as $cot) {
                    if ($cot->keytrans == 'name') {
                        $co->name = $cot->valuetrans;
                    }
    
                    if ($cot->keytrans == 'description') {
                        $co->description = $cot->valuetrans;
                    }
                }
            }

            $c->cookies = $cookies;
        }

        return $categories;
    }
    
    protected function saveCookie($cookiesAccept)
    {
        $cookiesAccept = empty($cookiesAccept) ? '' : implode(',', $cookiesAccept);
        $expire = \time() + 31557600; // one year expire cookie
        $this->response->headers->setCookie(new Cookie('acceptCookie', $cookiesAccept, $expire, FS_ROUTE));
    }
}