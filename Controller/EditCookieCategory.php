<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebCookies\Controller;

use FacturaScripts\Dinamic\Lib\ExtendedController\PanelController;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Model\CookieCategory;

/**
 * Description of EditCookieCategory
 *
 * @author Athos Online <info@athosonline.com>
 */
class EditCookieCategory extends PanelController
{
    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['menu'] = 'web';
        $pagedata['title'] = 'category';
        $pagedata['icon'] = 'fas fa-list-alt';
        $pagedata['showonmenu'] = false;
        return $pagedata;
    }

    /**
     * Load views.
     */
    protected function createViews()
    {
        $this->addEditView('EditCookieCategory', 'CookieCategory', 'category', 'fas fa-list-alt');
    
        if (WEBMULTILANGUAGE) {
            $this->addEditListView('EditCookieCategoryTranslate', 'WebTranslate', 'translations', 'fa fa-language');
            $this->setTabsPosition('top');
        }
    }

    protected function loadData($viewName, $view) {
        switch ($viewName) {
            case 'EditCookieCategory':
                $code = $this->request->get('code');
                $view->loadData($code);
                break;

                case 'EditCookieCategoryTranslate':
                    $modelLanguage = '\\FacturaScripts\\Dinamic\\Model\\WebLanguage';
                    // Languages
                    $columnLanguages = $this->views['EditCookieCategoryTranslate']->columnForName('language');
                    if ($columnLanguages && $columnLanguages->widget->getType() === 'select') {
                        $customValues = [];
                        foreach ($modelLanguage::getWebLanguages() as $lang) {
                            $customValues[] = [
                                'value' => $lang->codicu,
                                'title' => $lang->name
                            ];
                        }
                        $columnLanguages->widget->setValuesFromArray($customValues);
                    }

                    // Keys Fields translates
                    $columnKeys = $this->views['EditCookieCategoryTranslate']->columnForName('key');
                    if ($columnKeys && $columnKeys->widget->getType() === 'select') {
                        $customValues = [];
                        foreach (CookieCategory::$fieldsTranslate as $field) {
                            $customValues[] = [
                                'value' => $field,
                                'title' => $field
                            ];
                        }
                        $columnKeys->widget->setValuesFromArray($customValues);
                    }

                    $where = [
                        new DataBaseWhere('modelid', $this->request->get('code')),
                        new DataBaseWhere('modelname', 'CookieCategory')
                    ];
                    $view->loadData('', $where);
                    break;
        }
    }
}