<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebCookies\Model;

use FacturaScripts\Core\Model\Base;

/**
 * Description of CookieCategory
 *
 * @author Athos Online <info@athosonline.com>
 */
class CookieCategory extends Base\ModelClass
{
    use Base\ModelTrait;
    
    /**
     *
     * @var serial
     */
    public $idcategory;
    
    /**
     *
     * @var string
     */
    public $description;
    
    /**
     *
     * @var string
     */
    public $name;
    
    /**
     *
     * @var bool
     */
    public $necesary;

    public static $fieldsTranslate = ['name', 'description'];
    
    public static function primaryColumn()
    {
        return 'idcategory';
    }

    public static function tableName()
    {
        return 'cookies_categories';
    }
    
    /**
     *
     * @param string $type
     * @param string $list
     *
     * @return string
     */
    public function url(string $type = 'auto', string $list = 'List'): string
    {
        return parent::url($type, 'ListCookie?activetab=List');
    }
}