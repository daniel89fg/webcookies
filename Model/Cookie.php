<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebCookies\Model;

use FacturaScripts\Core\Model\Base;

/**
 * Description of CookieModel
 *
 * @author Athos Online <info@athosonline.com>
 */
class Cookie extends Base\ModelClass
{
    use Base\ModelTrait;
    
    /**
     *
     * @var serial
     */
    public $idcookie;
    
    /**
     *
     * @var integer
     */
    public $idcategory;
    
    /**
     *
     * @var string
     */
    public $description;
    
    /**
     *
     * @var integer
     */
    public $duration;
    
    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $time;

    public static $fieldsTranslate = ['description'];
    
    public static function primaryColumn()
    {
        return 'idcookie';
    }

    public static function tableName()
    {
        return 'cookies';
    }

    /**
     * Returns the description of the column that is the model's primary key.
     *
     * @return string
     */
    public function primaryDescriptionColumn()
    {
        return 'name';
    }

    /**
     * Load data from array
     *
     * @param array $data
     * @param array $exclude
     */
    public function loadFromData(array $data = [], array $exclude = [])
    {
        parent::loadFromData($data);
        $i18n = self::toolBox()->i18n();
        
        $time = $i18n->trans($this->time);
        if ($this->duration <> 1) {
            $time = $i18n->trans($this->time.'s');
        }

        $this->durationtime = $this->duration.' '.$time;
    }
}