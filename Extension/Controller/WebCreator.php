<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebCookies\Extension\Controller;

/**
 * Description of WebCreator
 *
 * @author Athos Online <info@athosonline.com>
 */
class WebCreator
{
    public function createViews() {
        return function() {
            $this->addEditView('WebSettingsCookies', 'Settings', 'cookies');
            $this->setSettings('WebSettingsCookies', 'btnDelete', false);

            if (WEBMULTILANGUAGE) {
                $this->views['WebSettingsCookies']->disableColumn('cookie-information', true);
            }
        };
    }

    public function loadData() {
        return function($viewName, $view) {
            switch ($viewName) {
                case 'WebSettingsCookies':
                    $view->loadData('webcreator');
                    $view->model->name = 'webcreator';
                    break;
            }
        };
    }
}